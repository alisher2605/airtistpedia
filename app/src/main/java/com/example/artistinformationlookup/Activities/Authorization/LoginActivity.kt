package com.example.artistinformationlookup.Activities.Authorization

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.mvrx.BaseMvRxViewModel
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.withState
import com.example.artistinformationlookup.Activities.MainActivity
import com.example.artistinformationlookup.Entities.UserItem
import com.example.artistinformationlookup.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

data class MainFragmentState(val user: UserItem) : MvRxState

class MainFragmentViewModel(initialState: MainFragmentState) :
    BaseMvRxViewModel<MainFragmentState>(initialState, debugMode = false) {
    fun login() {
        setState {
            copy(user = user)
        }
    }
}

class LoginActivity : AppCompatActivity() {
    private val auth by lazy { FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        title = "Sign In"
        goToRegister()
        signIn()
    }

    private fun goToRegister() {
        val intent = Intent(this, RegistrationActivity::class.java)
        go_to_register_button.setOnClickListener {
            startActivity(intent)
            finish()
        }
    }


    private fun signIn() {
        val intent = Intent(this, MainActivity::class.java)
        btnSignIn.setOnClickListener {
            if (login_username.text!!.isEmpty()) {
                login_username.error = getString(R.string.empty_username_error)
            }
            if (login_password.text!!.isEmpty()) {
                login_password.error = "Enter your password"
            }

            val email = login_username.text.toString().trim()
            val password = login_password.text.toString().trim()

            if (login_username.text!!.isNotEmpty() && login_password.text!!.isNotEmpty()) {
                auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Success!", Toast.LENGTH_LONG).show()
                        startActivity(intent)
                        finish()

                        val item: UserItem = UserItem()

                        item.email = login_username.text.toString()

                        val viewModel = MainFragmentViewModel(MainFragmentState(item))
                        viewModel.login()


                        withState(viewModel){state->
                            Toast.makeText(this, state.user.email, Toast.LENGTH_LONG).show()
                        }

                        return@addOnCompleteListener
                    }

                    Toast.makeText(this, task.exception?.message, Toast.LENGTH_LONG).show()
                }
            }
        }

    }
}
