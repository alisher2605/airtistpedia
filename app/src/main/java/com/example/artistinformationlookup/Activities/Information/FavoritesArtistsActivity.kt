package com.example.artistinformationlookup.Activities.Information

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artistinformationlookup.Activities.Authorization.LoginActivity
import com.example.artistinformationlookup.Activities.MainActivity
import com.example.artistinformationlookup.Adapters.FavoriteArtistAdapter
import com.example.artistinformationlookup.Networking.Responses.ArtistInfoItem
import com.example.artistinformationlookup.Entities.UserItem
import com.example.artistinformationlookup.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_favorites_artists.*
import kotlinx.android.synthetic.main.activity_favorites_artists.bottom_navigation
import kotlinx.android.synthetic.main.activity_main.*

class FavoritesArtistsActivity : AppCompatActivity() {
    private val database by lazy { FirebaseFirestore.getInstance() }
    private val auth by lazy { FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites_artists)
        loadFav(auth.currentUser!!.uid)

        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.ic_home -> {
                    // put your code here
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                    finish()

                    return@OnNavigationItemSelectedListener true
                }
                R.id.ic_favorites -> {
                    if (auth.currentUser == null){
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                    }
                    else{
                        val intent = Intent(this, FavoritesArtistsActivity::class.java)
                        startActivity(intent)
                    }

                    finish()

                    return@OnNavigationItemSelectedListener true
                }

            }
            false
        }

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
    private fun loadFav(uid: String){
        database.collection("users")
            .document(uid)
            .addSnapshotListener{snapshot, e->
                if (e != null){
                    Log.d("errrorr", e.localizedMessage)
                    return@addSnapshotListener
                }
                val user = snapshot?.toObject(UserItem::class.java)
                displayFavArtists(user!!.favoriteArtists)
            }
    }
    private fun displayFavArtists(artists: List<ArtistInfoItem>){
        favorite_artist_list.adapter = FavoriteArtistAdapter(
            artistList = artists, onClick = {
               val intent = Intent(this, ArtistInfoActivity::class.java)
                intent.putExtra("ARTISTNAME", it.artistName)
                startActivity(intent)
            })
        favorite_artist_list.layoutManager = LinearLayoutManager(this)
    }


}
